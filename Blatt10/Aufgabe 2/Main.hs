--Funktionen aus der Vorlesung
from :: Int -> [Int]
from x = x : from (x+1)

drop_mult :: Int -> [Int] -> [Int]
drop_mult x xs = filter (\y -> mod y x /= 0) xs

dropall :: [Int] -> [Int]
dropall (x:xs) = x : dropall (drop_mult x xs)

primes :: [Int]
primes = dropall (from 2)

--Funktionen aus der Aufgabenstellung
divisors  :: Int  -> [Int]
divisors x = filter  (\y -> mod x y == 0) [1.. div x 2]

-- Aufgaben
-- Alternative Lösung: eventlist = [0,2..]
evenlist :: [Int]
evenlist = map (\x -> 2*x) [0..]

perfect :: [Int]
perfect = filter (\ x -> x == sum (divisors x)) [2..]

primeFactors :: Int -> [Int]
primeFactors x = pHelper x primes
    where 
        pHelper :: Int -> [Int] -> [Int]
        pHelper y (x:xs) | y == 1 = []
                         | (mod y x) == 0 = x : pHelper (div y x) (x:xs)
                         | otherwise = pHelper y xs