vorlesung(db). 
vorlesung(da).
vorlesung(prog).
vorlesung(st).

seminar(psi).
seminar(si).

kenntnisseErforderlich(db, da).
kenntnisseErforderlich(da, prog).
kenntnisseErforderlich(psi, smo).
kenntnisseErforderlich(si, smo).
kenntnisseErforderlich(st, da).
kenntnisseErforderlich(st, prog).

pruefungErforderlich(si, psi).

% Teilaufgabe b) 
% kenntnisseErforderlich(st, X).

% Teilaufgabe c) 
% kenntnisseErforderlich(X, prog).

% Teilaufgabe d) 
% kenntnisseErforderlich(_, prog).

alleKenntnisse(X,Y) :- kenntnisseErforderlich(X,Y).
alleKenntnisse(X,Y) :- kenntnisseErforderlich(X,Z), alleKenntnisse(Z, Y).

voraussetzungen(X, Y) :- kenntnisseErforderlich(X,Y).
voraussetzungen(X, Y) :- pruefungErforderlich(X,Y).
