increment(leaf(N), leaf(s(N))).
increment(node(X,Y,Z), node(A, s(Y), B)) :- increment(X, A), increment(Z, B).

% Notation aus dem Überblickswissen [1|[1,2]]
append([], YS, YS).
append([X|XS], YS, [X|Res]) :- append(XS, YS, Res).

inorder(leaf(X), [X]).
inorder(node(L, Y, R), Res) :- inorder(L, LR), 
                               inorder(R, RR), 
                               append(LR, [Y], RY),
                               append(RY, RR, Res).