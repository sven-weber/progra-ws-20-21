public class Buerger {
    private String name; 

    public Buerger(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name;
    }

    public String getName()
    {
        return name;
    }

    public boolean hatDiebesgut()
    {
        return false;
    }

    public void aktion(Buerger[] buerger)
    {

    }
}
