public class ReicherBuerger extends Buerger {
    private int reichtum;

    public ReicherBuerger(String name, int reichtum)
    {
        super(name);
        this.reichtum = reichtum;
    }

    public int getReichtum()
    {
        return reichtum; 
    }
    
    public void setReichtum(int reichtum)
    {
        this.reichtum = reichtum; 
    }

    @Override
    public void aktion(Buerger[] buerger)
    {
        int bestechung = Zufall.zahl(this.reichtum);
        System.out.println("Reicher Buerger " + getName() + " besticht Politiker mit " + bestechung + " Euro");
        this.reichtum -= bestechung;
    }
}
