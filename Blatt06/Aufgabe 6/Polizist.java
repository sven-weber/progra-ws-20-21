public class Polizist extends Buerger{
    
    public Polizist(String name)
    {
        super(name);
    }

    @Override
    public void aktion(Buerger[] buerger)
    {
        for(int i = 0; i < buerger.length; i++)
        {
            if(buerger[i].hatDiebesgut())
            {
                buerger[i] = new Gefangener(buerger[i].getName());
                System.out.println("Polizist " + getName() + " verhaftet Dieb " + buerger[i].getName());
            }
        }   
    }
}
