import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;

public class Dieb extends Buerger {
    private int diebesgut = 0; 

    public Dieb(String name)
    {
        super(name);
    }

    @Override
    public boolean hatDiebesgut()
    {
        if(diebesgut > 0) return true; 
        else return false;
    }

    @Override
    public void aktion(Buerger[] buerger)
    {
        for(int i = 0; i < 5; i++)
        {
            Buerger b = buerger[Zufall.zahl(buerger.length)];
            if(b instanceof ReicherBuerger)
            {
                ReicherBuerger r = (ReicherBuerger)b;
                if(r.getReichtum() <= 1)
                {
                    continue;
                }
                int raubBetrag = Zufall.zahl(r.getReichtum());
                r.setReichtum(r.getReichtum() - raubBetrag);

                this.diebesgut = diebesgut + raubBetrag;
                System.out.println("Dieb " + getName() + " hat Reichen Bürger " + r.getName() + " um " + diebesgut + " Euro beklaut!");
                break;
            }
            if(b instanceof Polizist)
            {
                System.out.println("Dieb " + getName() + " hat Polizisten gefunden. Bricht suche ab ");
                break;
            }
        }
    }
}
