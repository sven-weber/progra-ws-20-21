public class Stadt {
    private Buerger[] buerger;
    
    public Stadt(int anzahl)
    {
        buerger = new Buerger[anzahl];
        for(int i = 0; i < buerger.length; i++)
        {
            buerger[i] = switch(Zufall.zahl(4))
            {
                case 0 -> new Dieb(Zufall.name()); 
                case 1 -> new Polizist(Zufall.name());
                case 2 -> new ReicherBuerger(Zufall.name(), Zufall.zahl(1000) + 1);
                default -> new Gefangener(Zufall.name());
            };
        }
    }

    public static void main(String[] args)
    {
        Stadt stadt = new Stadt(10); 
        for(int i = 0; i < 10; i++)
        {
            Buerger b = stadt.buerger[Zufall.zahl(stadt.buerger.length)]; 
            b.aktion(stadt.buerger);
        }
    }
}
