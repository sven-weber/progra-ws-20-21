public class Main {
  public static void main(String[] args) {
    Pilz steinpilz1 = new Pilz();
    steinpilz1.art = Pilzart.STEINPILZ;

    Pilz steinpilz2 = new Pilz();
    steinpilz2.art = Pilzart.STEINPILZ;

    Pilz champignon = new Pilz();
    champignon.art = Pilzart.CHAMPIGNON;

    Pilz pfifferling = new Pilz();
    pfifferling.art = Pilzart.PFIFFERLING;

    Pilzsammler pettersson = new Pilzsammler();
    pettersson.name = "Pettersson"; 
    pettersson.korb = new Pilz[8]; 

    Pilzsammler findus = new Pilzsammler();
    findus.name = "Findus"; 
    findus.korb = new Pilz[7];

    //Wir sammeln Pilze
    Pilz[] rest = findus.sammlePilze(steinpilz1, steinpilz2, champignon, pfifferling);
    pettersson.sammlePilze(rest);
    findus.ausgabe();
    pettersson.ausgabe();
    System.out.println("---");

    while(findus.hatPlatz() || pettersson.hatPlatz())
    {
      Pilz[] lichtung = Pilz.Pilzlichtung();
      Pilz[] rest2 = findus.sammlePilze(lichtung); 
      pettersson.sammlePilze(rest2);
      findus.ausgabe();
      pettersson.ausgabe();
      System.out.println("---");
    }
  }
}
