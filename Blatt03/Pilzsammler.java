public class Pilzsammler {
  String name;
  Pilz[] korb;
  int anzahl = 0;

  public Pilz[] sammlePilze(Pilz... pilze) {
    int nichtSammelbar =  pilze.length - (korb.length - anzahl);
    if(nichtSammelbar < 0)
    {
       nichtSammelbar = 0; 
    }
    Pilz[] rest = new Pilz[nichtSammelbar];
    int iRest = 0; 
    for(int i = 0; i < pilze.length; i++)
    {
      if (hatPlatz())
      {
        korb[anzahl] = pilze[i]; 
        anzahl++; 
        System.out.println(name  +" sammelt einen " + pilze[i].art);
      } else 
      {
        rest[iRest] = pilze[i];
        iRest++;
        System.out.println(name  +" hat keinen Platz mehr für " + pilze[i].art);
      }
    }
    return rest;
  }

  public boolean hatPlatz() {
      if(anzahl < korb.length)
      {
        return true;
      }
      return false;
  }

  public void ausgabe() {
      System.out.println(name + "(" + anzahl + "):");
      for(Pilz pilz : korb)
      {
        if (pilz != null)
        {
            System.out.println(pilz.art);
        }
      }
  }
}
