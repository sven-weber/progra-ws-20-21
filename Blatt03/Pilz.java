public class Pilz {
  Pilzart art;

  public static Pilz[] Pilzlichtung() {
      Pilzart[] pilze = Pilzart.values();
      Pilz[] result = new Pilz[pilze.length];
      int i = 0;
      while(i < pilze.length)
      {
        result[i] = new Pilz(); 
        result[i].art = pilze[i];
        i++;
      }
      return result;
  }

}
