public class Toolbox {
    
    /**
     * Konstante, die aussagt, wie viel Platz ein PowetTool verbraucht
     */
    public static final int PowerToolSize = 3;  

    /**
     * Array, das Faecher der Toolbox repraesentiert
     */
    private Tool[] tools;

    /**
     * Wert, wie viele Faecher noch in der Toolbox frei sind
     */
    private int capacity; 

    /**
     * Name der Toolbox
     */
    private String name; 

    /**
     * Erzeugt eine neue Toolbox mit der gegebenen Kapazitaet
     * @param name Der Name der Toolbox
     * @param capacity Die Kapazitaet der Toolbox
     */
    public Toolbox(String name, int capacity)
    {
        this.name = name; 
        this.capacity = capacity;
        tools = new Tool[capacity];
    }

    /**
     * Erzeugt eine neue Toolbox aus den übergebenen Tools
     * @param name Der Name der Toolbox
     * @param tools Die Tools, die sich in der Toolbox befinden sollen
     */
    public Toolbox(String name, Tool ... tools)
    {
        this.name = name;
        this.capacity = 0;
        this.tools = tools; 
        for(Tool tool : tools)
        {
            if(tool == null)
            {
                this.capacity++;
            }
        }
    }

    /**
     * Gibt die aktuelle Kapaziteat der Toolbox zurück
     * @return die aktuelle Kapaziteat der Toolbox
     */
    public int getCapacity()
    {
        return capacity;
    }
    
    /**
     * Gibt den namen der toolbox zurück
     * @return den namen der toolbox
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Setzt den Namen der Toolbox auf den gegebenen Wert
     * @param name Neuer Name der Toolbox
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gibt das tool im gegebenen Fach zurueck
     * @param fach Der index des fachs, von dem das tool zurueckgegeben werden soll
     * @return Gibt das tool im gegebenen Fach zurueck, oder null wenn das fach invalid war
     */
    public Tool getTool(int fach)
    {
        if(fach < tools.length && fach >= 0)
        {
            return tools[fach]; 
        } 
        return null;
    }

    /**
     * Ueberprueft, ob noch Platz fuer ein Powertool ist
     * @param wrapper Wrapper Object zum speichern des Index
     * @return true, wenn noch Platz ist, sonst false. Wenn true steht der Index im Wrapper
     */
    private boolean checkRoomForPowerTool(Wrapper wrapper)
    {
        int currentLenght = 0; 
        int currentIndex = -1;
        for(int i = 0; i < tools.length; i++)
        {
            if(tools[i] == null)
            {
                currentLenght++;
                if(currentLenght == 1) 
                {
                    currentIndex = i; 
                }

                if(currentLenght == PowerToolSize)
                {
                    wrapper.set(currentIndex);
                    return true; 
                }
            } else
            {
                currentIndex = -1;
                currentLenght = 0;
            }
        }
        return false;
    }

    public void addTool(Tool tool)
    {
        switch(tool)
        {
            case PowerTool -> {
                Wrapper wrapper = new Wrapper(0); 
                if(checkRoomForPowerTool(wrapper))
                { 
                    //[SimpleTool, Materials, null, Materials, null, null, null]
                    for(int i = 0; i < PowerToolSize; i++)
                    {
                        tools[i + wrapper.get()] = Tool.PowerTool;
                    }
                    capacity -= PowerToolSize;
                }
            }
            case SimpleTool -> 
            {
                for(int i = 0; i < tools.length; i++)
                {
                    if(tools[i] == null)
                    {
                        tools[i] = Tool.SimpleTool; 
                        capacity -= 1; 
                        break;
                    }
                }
            }
            case Materials -> {
                for(int i = 0; i < tools.length; i++)
                {
                    if(tools[i] == null)
                    {
                        tools[i] = Tool.Materials; 
                        capacity -= 1; 
                        break;
                    } else if (tools[i] == Tool.Materials)
                    {
                        break;
                    }
                }
            }
        }
    }
}
