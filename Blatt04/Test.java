/**
     * Diese Klasse und alle Methoden sind in der Aufgabe nicht gefodert
     * Wir verwenden sie nur zum Testen um Tutorium
     */
public class Test {

    private static int capacity = 10; 

    public static void main(String[] args)
    {
        Toolbox toolbox = new Toolbox("MyToolbox", capacity); 

        toolbox.addTool(Tool.Materials);
        toolbox.addTool(Tool.PowerTool);
        toolbox.addTool(Tool.Materials);
        printToolbox(toolbox);
    }

    public static void printToolbox(Toolbox box)
    {
        System.out.println(box.getName() + ":"); 
        for(int i = 0; i < capacity; i++)
        {
            System.out.println("[" + i + "]: " + box.getTool(i));
        }
    }
}
