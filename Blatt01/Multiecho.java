public class Multiecho {
 
    public static void main(String [] args)
    {
        int zahl = 0; 
        while(zahl <= 0)
        {
            zahl = SimpleIO.getInt("Bitte eine positive Zahl:");
        }

        String eingabe = SimpleIO.getString("Bitte ein Wort eingeben");
        String ausgabe = ""; 
        for(int i = 0; i < zahl; i++)
        {
            ausgabe += eingabe; 
        }
        SimpleIO.output(ausgabe, "Multiecho");
    }
}
