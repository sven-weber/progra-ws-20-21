fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib x = fib(x-1) + fib(x-2)

primeHelp :: Int -> Int -> Bool
primeHelp _ 1 = True
primeHelp x y = ((rem x y) /= 0) && primeHelp x (y-1)

prime :: Int -> Bool
prime 0 = False
prime 1 = False
prime n = primeHelp n (n-1)

powersOfTwo :: Int -> Int -> [Int]
powersOfTwo x y | x > y = []
                | otherwise = 2^x : powersOfTwo (x+1) y

contains :: [Int] -> Int -> Bool
contains [] _ = False
contains (x:xs) y | x == y = True
                  | otherwise = contains xs y

-- Zweiter Fall (_ []) kann auch weg gelassen werden (ist in contains)
intersection :: [Int] -> [Int] -> [Int]
intersection [] _ = []
intersection _ [] = []
intersection (x:xs) ys | contains ys x = x : intersection xs ys
                       | otherwise = intersection xs ys

split :: Int -> [Int] -> ([Int], [Int])
split _ [] = ([], [])
split pivot (x:xs) = if (x > pivot) then (left, x : right) else (x :left, right)
    where (left, right) = split pivot xs

selectKsmallest :: Int -> [Int] -> Int
selectKsmallest k (pivot:xs) = 
    let 
        left, right :: [Int]
        (left, right) = split pivot xs
        lenght_left :: Int
        lenght_left = length left
        in 
            if(lenght_left == (k-1)) then pivot else
                if(lenght_left > (k-1)) then selectKsmallest k left else 
                    selectKsmallest (k-1-lenght_left) right