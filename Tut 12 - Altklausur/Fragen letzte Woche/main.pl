% Unterschied zwischen =\ und =\=

% \= meint nicht unifizierbar, das ist genau dann wahr
% Wenn es keine Möglichkeit gibt, die Terme auf beiden Seiten zu unifizieren
% z.B. notdivisor(10,5)
% Frage: Gibt es eine Unifikation für (10 mod 5) = 0? 
% mod ist arithmetisches Prädikat, diese werden bei Unifikation nicht ausgewertet
% Also: Es gibt keine Möglichkeit(nur Konstanten und Arithmetik), 10 mod 5 \= 0 also wahr
notdivisor(X,Y) :- (X mod Y) \= 0.

% =\= meint Arithmetrisch gleich
% z.B. notdivisor(10,5)
% Frage: Ist 10 mod 5 arithmetrisch gleich zu 0? 
% 10 mod 5 => 0, also wahr
% damit notdivisor falsch.
%notdivisor(X,Y) :- (X mod Y) =\= 0.

%Warum liefern folgende Ausdrücke nicht das gewünschte Ergebnis?
%1.
% Hier wird kein arithmetrisches Prädikat verwendet 
% (deshalb wird Y-1 nicht ausgewertet, sondern es wird versucht das so zu unifizieren)
notdivisors(X, Y) :- notdivisors(X, Y-1).

%2.
% Das geht grundsätzlich, aber: Datenstruktur mit successor und 0
% unterscheidet sich z.B. von 1,2,3 (der direkten Verwendung von natürlichen Zahlen)
notdivisors(X, s(Y)) :- notdivisors(X, Y).
