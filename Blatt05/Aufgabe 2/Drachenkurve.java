public class Drachenkurve {

	public static void kurveR(Canvas c, int ordnung)
	{
		if(ordnung == 0)
		{
			c.drawForward(10);
		} else 
		{
			kurveR(c, ordnung-1);
			c.rotate(90);
			kurveL(c, ordnung-1);
		}
	}

	public static void kurveL(Canvas c, int ordnung)
	{
		if(ordnung == 0)
		{
			c.drawForward(10);
		} else 
		{
			kurveR(c, ordnung-1);
			c.rotate(-90);
			kurveL(c, ordnung-1);
		}
	}


	public static void main(String[] args) {
		Canvas s = new Canvas();
		s.rotate(180); // Rotiert die aktuelle Ausrichtung nach oben
		kurveR(s, 10);
	}
}
