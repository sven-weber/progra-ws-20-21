public class List {
    
    static final List EMPTY = new List(null, 0);

    private final List nextList; 
    private final int value;

    public List(List nextList, int value)
    {
        this.value = value; 
        this.nextList = nextList;
    }

    public int getValue()
    {
        return value;
    }

    public List getNext()
    {
        return this.nextList;
    }

    public boolean isEmpty()
    {
        return this == EMPTY;
    }

    /**
     * Gibt die Laenge der liste, auf die lengt() augerufen wird zurueck
     * @return Die Laenge der liste, auf die length() aufgerufen wird
     */
    public int length()
    {
        if(isEmpty())
        {
            return 0;
        }
        return 1 + getNext().length();
    }

    /**
     * @return Eine kommaseperatierte String Repraesentation der Liste
     */
    public String toString()
    {
        if(!isEmpty())
        {
            if(getNext().isEmpty())
            {
                return String.valueOf(this.value);
            } else
            {
                return this.value + "," + getNext().toString();
            }
        } else
        {
            return "";
        }
    }

    /**
     * 
     * @param elementCount Anzahl der ELemente der neuen Subliste
     * @return Eine Subliste der aktuellen Liste mit elementCount elementen, oder die aktuelle Liste wenn die elementCount kleiner der Länge der Liste
     */
    public List getSublist(int elementCount)
    {
        if(elementCount >= length())
        {
            return this;
        }
        if(isEmpty() || elementCount <= 0)
        {
            return EMPTY; 
        } else
        {
            List list = getNext().getSublist(elementCount-1);
            return new List(list, getValue()); 
        }
    }
}
