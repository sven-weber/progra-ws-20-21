public class Animals {
    //Die folgende Methode ist nur als Test gedacht,
    //sie ist nicht noetig fuer die Loesung	
    public static void main(String[] args) {
        String[] tiere = {"Elefant","Maus", "Elefant", "Tiger", "Affe", "Tiger", "Hund"}; //Futterbedarf: 12+1+12+5+3+5+(4)= 42
        futterBedarf(tiere);
    }

	public static void futterBedarf(String[] tiere) {
        int summe = 0;
        for(String tier : tiere)
        {
            summe += switch(tier)
            {
                case "Elefant" -> 12;
                case "Tiger" -> 5; 
                case "Affe" -> 3; 
                case "Maus" -> 1; 
                default -> 4;
            };
        }
        System.out.println("Die Summe ist: " + summe);
	}
}
