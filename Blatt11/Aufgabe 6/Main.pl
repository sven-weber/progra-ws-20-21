% Soll wahr sein, wenn Y nicht X teilt.
notdivisor(X,Y) :- X mod Y =\= 0.

notdivisors(_, 1).
notdivisors(X, Y) :- notdivisor(X, Y), Z is Y-1, notdivisors(X, Z).

prime(X) :- X > 1, Y is X-1, notdivisors(X, Y).