data VariableName = X | Y deriving Show

getValue :: VariableName -> Int
getValue X = 5
getValue Y = 13

data Expression = Constant Int
                | Variable VariableName
                | Add Expression Expression
                | Multiply Expression Expression
                deriving Show

evaluate :: Expression -> Int
evaluate (Constant x)   = x
evaluate (Variable x)   = getValue x
evaluate (Add x y)      = (evaluate x) + (evaluate y)
evaluate (Multiply x y)  = (evaluate x) * (evaluate y)