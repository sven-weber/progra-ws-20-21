-- Code aus der Aufgabenstellung
data List a = Nil | Cons a (List a) deriving Show

list :: List  Int
list = Cons (-3) (Cons 14 (Cons (-6) (Cons 7 (Cons 1 Nil ))))

blist :: List  Int
blist = Cons 1 (Cons 1 (Cons 0 (Cons 0 Nil)))

-- Aufgaben
filterList :: (a -> Bool) -> List a -> List a
filterList x Nil = Nil
filterList x (Cons y z) | x y = Cons y rest
                        | otherwise = rest
                    where rest = filterList x z

divisibleBy :: Int -> List Int -> List Int
divisibleBy x xs = filterList (\y -> rem y x == 0) xs

foldList :: (a -> b -> b) -> b -> List a -> b
foldList _ x Nil = x
foldList x zero (Cons y ys) = x y (foldList x zero ys)

listMaximum :: List Int -> Int
listMaximum x = foldList max minBound x
    where max = (\x y -> if x > y then x else y)

mapList :: (a -> b) -> List a -> List b
mapList f y = foldList (\x y -> Cons (f x) y) Nil y

zipLists :: (a -> b -> c) -> List a -> List b -> List c
zipLists _ Nil _ = Nil
zipLists _ _ Nil = Nil
zipLists f (Cons x xs) (Cons y ys) = Cons (f x y) (zipLists f xs ys)